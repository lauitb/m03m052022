package uf2.classfun

import java.util.*
import kotlin.math.pow


fun main() {
    val scanner = Scanner(System.`in`).useLocale(Locale.US)
    val numRectangles = scanner.nextInt()

    var rightTriangles = mutableListOf<RightTriangleSize>()
    readAllRihtTriangles(scanner, rightTriangles, numRectangles)
    showInfo(rightTriangles)
}

fun readAllRihtTriangles(scanner: Scanner, rightTriangles: MutableList<RightTriangleSize>, numRectangles: Int) {
    repeat(numRectangles) {
        var rightTriangle: RightTriangleSize = readRectangle(scanner)
        rightTriangles += rightTriangle
    }
}

fun showInfo(rightTriangles: MutableList<RightTriangleSize>) {
    showPerimeter(rightTriangles)
    showArea(rightTriangles)
    showRectangle(rightTriangles)
}

/**
 *
2
2.0 4.0
2.0 4.0
 */

fun showRectangle(rightTriangles: MutableList<RightTriangleSize>) {
    for (rightTriangle in rightTriangles){
        println(rightTriangle.toString())
    }
}

fun showArea(rightTriangles: MutableList<RightTriangleSize>) {
    for (rightTriangle in rightTriangles){
        print("${rightTriangle.area} ")
    }
    println()
}

fun showPerimeter(rightTriangles: MutableList<RightTriangleSize>) {
    for (rightTriangle in rightTriangles) {
        print("${rightTriangle.perimeter} ")
    }
    println()
}

fun readRectangle(scanner: Scanner): RightTriangleSize {
    var width = scanner.nextDouble()
    var height = scanner.nextDouble()
    return RightTriangleSize(width, height)
}


data class RightTriangleSize(var width: Double, val height: Double) {
    val area get() = 1/2*width*height
    val perimeter get() = width + height+ Math.sqrt(width.pow(2)+height.pow(2))

}
