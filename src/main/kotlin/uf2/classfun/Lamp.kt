package uf2.classfun

import java.util.*

class Lamp(var isOn: Boolean = false) {
   // val off get() = !on

    fun toggle(){
        this.isOn = !isOn
    }

    fun turnOn(){
        isOn = true
    }

    fun turnOff(){
        isOn = false
    }
}

fun main() {
    val scanner = Scanner(System.`in`)
    var userInput = readUserInfo(scanner)
    val lamp = Lamp()
    while (userInput != "END") {
        touchLamp(userInput, lamp)
        showLampEstate(lamp)
        userInput = readUserInfo(scanner)
    }


}

fun readUserInfo(scanner: Scanner): String {
    return scanner.next()
}

fun showLampEstate(lamp: Lamp) {
   println(lamp.isOn)
}

fun touchLamp(action: String, lamp: Lamp) {
    when (action){
        "TURN OFF" -> lamp.turnOff()
        "TURN ON" -> lamp.turnOn()
        "TOGGLE" -> lamp.toggle()
    }
}
