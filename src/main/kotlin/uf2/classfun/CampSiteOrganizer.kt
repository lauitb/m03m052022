package uf2.classfun

import java.util.*

class CampSiteOrganizer(){
    var person = 0
    var reservations = mutableListOf<Reservation>()
    val busySites get()= reservations.size

    /**
     * Add reservation into reservations and update number of persons in camping
     */
    fun addReservation(reservation: Reservation){
        reservations.add(reservation)
        person+=reservation.persons
    }

    /**
     * remoffdkhjfgjhfjfhg sgrgfdsh
     */
    fun deleteReservation(name: String){
        for (reserva in reservations){
            if (reserva.name == name){
                reservations.remove(reserva)
                person-=reserva.persons
            }
        }
    }

}
class Reservation(var name:String, var persons:Int ){

}

fun main() {
    val scanner = Scanner(System.`in`)
    var line = scanner.next()
    val campSiteOrganizer = CampSiteOrganizer()
    while (line!="END"){
        getUserCommand(line, campSiteOrganizer)
        line = scanner.next()
    }
}

fun getUserCommand(line: String, campSiteOrganizer: CampSiteOrganizer) {
    if (line.startsWith("ENTRA")){
        val (_,people,name) =line.split(" ")
        val reservation = Reservation(name, people.toInt())
        campSiteOrganizer.addReservation(reservation)
        showInformation(campSiteOrganizer)
    }
    else {
        //MARXA
        val(_,name) =line.split(" ")
        campSiteOrganizer.deleteReservation(name)
        showInformation(campSiteOrganizer)
    }
}

fun showInformation(campSiteOrganizer: CampSiteOrganizer) {
    println("parcel·les: ${campSiteOrganizer.busySites}")
    println("persones: ${campSiteOrganizer.person}")
}
