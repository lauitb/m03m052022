package uf2.functions

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    var teamHost = readTeam(scanner)
    var teamVisitor = readTeam(scanner)
    var line = scanner.nextLine()
    var lastScore = ScoreBoard()
    while (line != "-1") {
        var currentScore: ScoreBoard = getScore(line)
        var teamScored: Team = getTeamWhoScored(lastScore, currentScore)
        var typeBasket = getTypeOfBasket(lastScore, currentScore, teamScored)
        showMessage(teamScored, typeBasket, teamHost, teamVisitor)
        lastScore = currentScore
        line = scanner.nextLine()
    }
    showWinner(lastScore, teamHost, teamVisitor)
}

fun showWinner(lastScore: ScoreBoard, teamHost: String, teamVisitor: String) {
    var message = if (lastScore.host > lastScore.visitor) "Guanya $teamHost"
    else "Guanya $teamVisitor"
    println(message)
}

fun showMessage(teamScored: Team, typeBasket: Int, teamHost: String, teamVisitor: String) {
    //Cistella de Manresa
    var message = ""
    when(typeBasket){
        1 -> message = "Tir lliure de "
        2 -> message = "Cistella de "
        3 -> message = "Triple de "
    }
    if (teamScored == Team.HOST){
        message +="$teamHost"
    } else message +="$teamVisitor"
    println(message)
}

fun getTypeOfBasket(lastScore: ScoreBoard, currentScore: ScoreBoard, teamScored: Team): Int {
    var type:Int
    if (teamScored == Team.HOST){
        type = currentScore.host - lastScore.host
    }else{
        type = currentScore.visitor - lastScore.visitor
    }
    return type
}

fun getTeamWhoScored(lastScore: ScoreBoard, currentScore: ScoreBoard): Team {
    if (lastScore.host != currentScore.host) return Team.HOST
    else return Team.VISITOR
}

/**
 * Get Score from a string that contains two ints separated by blank
 */
fun getScore(line: String): ScoreBoard {
    var (host, visitor) = line.split(" ").map { it.toInt() }
    var scoreBoard = ScoreBoard(host, visitor)
    return scoreBoard
}

/**
 * reads the name of the team from scanner
 */
fun readTeam(scanner: Scanner): String {
    return scanner.nextLine()
}
