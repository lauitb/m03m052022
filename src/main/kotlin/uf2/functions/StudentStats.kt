package uf2

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val marks = readIntList(scanner)
    val minMark= min(marks)
    println(minMark)
    val maxMark = max(marks)
    println(maxMark)
    val avgMark = avg(marks)
    println(avgMark)
}