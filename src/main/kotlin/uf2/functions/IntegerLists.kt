package uf2

/**
 * min of a list, the list has at least one element
 */
fun min(list: List<Int>): Int {
    var min = list[0]
    for (element in list) {
        if (element < min) min = element
    }
    return min
}

fun max(list: List<Int>): Int {
    var max = list[0]
    for (element in list) {
        if (element > max) max = element
    }
    return max
}

fun avg(list: List<Int>): Double {
    var sum = 0
    for (element in list) {
        sum += element
    }
    return sum.toDouble()/list.size
}