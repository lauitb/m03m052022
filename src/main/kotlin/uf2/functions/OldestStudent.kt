package uf2

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val agesStudents = readIntList(scanner)
    val oldestStudent = max(agesStudents)
    println(oldestStudent)
}