package uf2.functions.uf2



fun main() {

    var rectangle = RectangleT(1,2)
    rectangle.rotate()
    println(rectangle.toString())
    var rectangle2 = RectangleT(1,2)
    println(rectangle == rectangle2)
}


 data class RectangleT(var width: Int, var height: Int) {
    val area get() = this.width * this.height

    fun rotate(){
        var width = this.width
        this.width = this.height
        this.height = width
    }
}

