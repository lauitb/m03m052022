package uf2


/**
 *  Calculate how much money a cashier will have at the end of the
 *  workday
 * startAmount is an initial sum in the cash desk.
 * ticketPrice is the price of a ticket.
 * soldTickets is the number of tickets sold on this day.
 **/
fun calcEndDayAmount(startAmount: Int, ticketPrice: Int, soldTickets: Int) =
    startAmount + ticketPrice * soldTickets

fun main() {
    val amount = calcEndDayAmount(100,399,200)
// Using named arguments you can also change the order of arguments
    val amount2 = calcEndDayAmount(
        ticketPrice = 10,
        startAmount = 1000,
        soldTickets = 500
    )
    calcEndDayAmount(100,1,10)
}
