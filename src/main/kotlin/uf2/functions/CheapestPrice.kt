package uf2

import java.util.*


fun main() {
    val scanner = Scanner(System.`in`)
    val prices = readIntList(scanner)
    val cheapest = min(prices)
    println(cheapest)
}