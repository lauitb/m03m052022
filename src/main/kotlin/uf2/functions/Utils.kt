package uf2

import java.util.*

fun readIntList(scanner: Scanner): List<Int> {
    val list = mutableListOf<Int>()
    var next = scanner.nextInt()
    while (next !=-1){
        list+=next
        next = scanner.nextInt()
    }
    return list
}