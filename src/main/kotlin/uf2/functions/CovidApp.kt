package uf2

import java.util.*

/**
 * nombre de casos total
 * última taxa de creixement
 * mitjana dels creixements
 */
fun main() {
    val scanner = Scanner(System.`in`)
    val cases = readDailyCasesFromScanner(scanner)
    if (cases!=null ){
        val total = countTotalCases(cases)
        println(total)
        val growths = growthRates(cases)
        println(growths[growths.lastIndex])
        //avg(growths)
    }


}