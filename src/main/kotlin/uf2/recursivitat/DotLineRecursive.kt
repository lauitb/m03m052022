package uf2.recursivitat

import java.util.*

fun dotLineRecursive(dots: Int): String {
    if (dots == 0) {
        return ""
    } else {
        var result = "." + dotLineRecursive(dots - 1)
        return result
    }
}

fun main() {
    val scanner = Scanner(System.`in`)
    val userInputValue = scanner.nextInt()

    val result = dotLineRecursive(userInputValue)
    println(result)

}