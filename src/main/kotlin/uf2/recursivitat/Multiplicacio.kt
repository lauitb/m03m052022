package uf2.recursivitat

import java.util.*


fun main() {
    val scanner = Scanner(System.`in`)
    val a = scanner.nextInt()
    val b = scanner.nextInt()

    //val result = multiplicacio(a,b)
    val result = factorial(a)
    println(result)
}

/**
 * numeros naturals
 */
fun factorial(a: Int): Int {
    if (a == 1) return 1
    else {
        val b = factorial(a-1)
        return a*b
    }



}

fun multiplicacio(a: Int, b: Int): Int {
    if (b == 1) return a
    else return a + multiplicacio(a, b - 1)

}
