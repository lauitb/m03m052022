package uf5

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    var products = readProducts(scanner)

    totalPriceLlisto(products)
    lengthExpensiveProduct(products)
    countProductsExpensiveThan (products, 100)

}

fun countProductsExpensiveThan(products: List<Product>, price: Int) {
    //var count = products.filter { it.totalPrice > price }.size
    var count = products.count { it.totalPrice > price }

    println("Num productés de més de $price€: $count")
}

fun lengthExpensiveProduct(products: List<Product>) {
    var maxPrice = products.maxOf { it.totalPrice }
    var length = products.find { it.totalPrice == maxPrice }?.lenght
    println("Producte més car té una llargada de : $length")
}

fun totalPriceLlisto(products: List<Product>) {
    var sum = products.filter { product -> product is Llisto }.sumOf { it.totalPrice }
    println("Preu total llistons: $sum")
}
