package uf5

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    var countries = readAllCountries(scanner)
    var countriesMayus = countries.map { it.name.toUpperCase() }
    var avgPopulation = getCountryDataSortAveragePopulation(countries, 1200000)
    println(countriesMayus)

}

fun getCountryDataSortAveragePopulation(countries: List<Country>, surface: Int): Int {
    var countriesFiltered = countries.filter { it.surface < surface }
    return if (countriesFiltered.isNotEmpty()){
        //countries.map { it.density*it.surface }.average()
        countriesFiltered.sumOf { it.density * it.surface } / countriesFiltered.size
    }
    else 0
}

fun readAllCountries(scanner: Scanner): List<Country> {
    val numCountries = scanner.nextInt()
    scanner.nextLine()

    var countries = listOf<Country>()
    repeat(numCountries) {
        countries += readCountryL(scanner)
    }
    return countries
}

fun readCountryL(scanner: Scanner): Country {
    return Country(scanner.next().toString(), scanner.next().toString(), scanner.next().toInt(), scanner.next().toInt())

}
