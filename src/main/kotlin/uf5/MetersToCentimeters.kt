package uf5

import uf2.readIntList
import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)

    var distances = readIntListUf5(scanner)
    metersToCentimeters(distances)
}

fun readIntListUf5(scanner: Scanner): MutableList<Int> {
    var total = scanner.nextInt()
    return  MutableList<Int>(total){scanner.nextInt()}
}

fun metersToCentimeters(distances: List<Int>) {
    var distancesInCentimeters = distances.map { it * 1000 }
    distancesInCentimeters.forEach { println(it) }
}
