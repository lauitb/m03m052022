package uf5

import java.util.*

 data class Country(var name: String, var capital: String, var surface: Int, var density: Int) {
}

fun main() {

    val scanner = Scanner(System.`in`)

    var countries = readCountries(scanner)
    countriesHighDensitySurface(countries, 1000, 5)
}

fun countriesHighDensitySurface(countries: List<Country>, surface: Int, density: Int) {
    var result = countries.filter { it.surface > surface && it.density > density }.sortedBy { it.capital }
    result.forEach { country -> println(country.name) }
}

fun readCountries(scanner: Scanner): List<Country> {
    var countries = mutableListOf<Country>()
    val totalCounries = scanner.nextInt()
    scanner.nextLine()
    repeat(totalCounries) {
        countries.add(readCountry(scanner))
    }
    return countries
}

fun readCountry(scanner: Scanner): Country {
    return Country(scanner.nextLine().toString(), scanner.nextLine().toString(), scanner.nextLine().toInt(), scanner.nextLine().toInt())
}


