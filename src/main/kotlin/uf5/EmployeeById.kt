package uf5

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val employees = readEmployees(scanner)
    val employeesV2 = readEmployeesV2(scanner)

    var next= scanner.nextLine()
    while (next!="END"){
        println(employees.get(next))
        next= scanner.nextLine()
    }
}

fun readEmployees(scanner: Scanner): Map<String, Employee> {
    val numTotalEmployees = scanner.nextLine().toInt()
    val employees = mutableMapOf<String,Employee>()
    repeat(numTotalEmployees){
        val employee = readEmployee(scanner)
        employees.put(employee.dni,employee)
    }
    return employees
}

fun readEmployeesV2(scanner: Scanner): Map<String, Employee> {
    val numTotalEmployees = scanner.nextLine().toInt()
    val employees = mutableListOf<Employee>()
    repeat(numTotalEmployees){
        employees+= readEmployee(scanner)
    }
    val employeesMap = employees.associateBy { it.dni }
    return employeesMap
}

fun readEmployee(scanner: Scanner): Employee{
    return Employee(scanner.nextLine(),scanner.nextLine(),scanner.nextLine(),scanner.nextLine())
}

data class Employee (var dni:String, var name:String,var surname:String,var address:String){

}
