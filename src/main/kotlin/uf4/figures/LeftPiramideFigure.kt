package uf4.figures

class LeftPiramidFigure(var base: Int,  color: String):Figure(color) {

    fun paint() {
        prepareColor()
        for (i in 0..base-1) {
            repeat(i){
                print("X")
            }
            println()
        }
        clearColor()
    }
}