package uf4.figures

class RectangleFigure(var width: Int, var heigth: Int, color: String):Figure(color) {
    fun paint() {
        prepareColor()
        for (i in 0..heigth - 1) {
            for (j in 0..width - 1) {
                print("X")
            }
            println()
        }
        clearColor()
    }
}



fun main() {
    var quadrat = RectangleFigure(4, 4, RED)
    var rect = RectangleFigure(4, 2, GREEN)
    var yell = LeftPiramidFigure(3, YELLOW)

    quadrat.paint()
    rect.paint()
    yell.paint()
}