package uf4

class Exemple2 {

}

data class Cotxe(var name:String, var matricula:String, var horsePower:Int) : Comparable<Cotxe>{
    override fun compareTo(other: Cotxe): Int {
        return this.horsePower - other.horsePower
        // 200 - 120 = 80
    }
}

fun main() {
    var cotxeLau= Cotxe("Wolkswagen", "4335JTT", 120)
    var cotxeSwarim= Cotxe("Ferrari", "4335JLT", 220)
    var cotxeCarlos= Cotxe("Twingo", "1235JLT", 220)
    var cotxeYess= Cotxe("Buggatti", "2235JLT", 200)
    var cotxes = listOf<Cotxe>(cotxeLau, cotxeCarlos, cotxeSwarim, cotxeYess)
    cotxeYess.compareTo(cotxeLau)
    cotxes.sorted()

    println (cotxes)
}