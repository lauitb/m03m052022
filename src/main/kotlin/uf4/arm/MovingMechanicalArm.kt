package uf4.arm

class MovingMechanicalArm(turnedOn: Boolean = false, openAngle: Double = 0.0, altitude: Double = 0.0, var position: Int = 0) :
    MechanicalArm(turnedOn, openAngle, altitude) {

 fun move(metersMoved:Int){
     //comprovar limits
     position +=metersMoved
 }
}