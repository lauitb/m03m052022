package uf4.arm


fun main() {
//    MechanicalArm{openAngle=0.0, altitude=0.0, turnedOn=true}
//    MechanicalArm{openAngle=0.0, altitude=3.0, turnedOn=true}
//    MechanicalArm{openAngle=180.0, altitude=3.0, turnedOn=true}
//    MechanicalArm{openAngle=180.0, altitude=0.0, turnedOn=true}
//    MechanicalArm{openAngle=0.0, altitude=0.0, turnedOn=true}
//    MechanicalArm{openAngle=0.0, altitude=3.0, turnedOn=true}
//    MechanicalArm{openAngle=0.0, altitude=3.0, turnedOn=false}
    val arm = MechanicalArm()
    arm.toggle()
    println(arm)
    arm.updateAltitude(3.0)
    println(arm)
    arm.updateAngle(180.0)
    println(arm)
    arm.updateAltitude(-3.0)
    println(arm)
    arm.updateAngle(-180.0)
    println(arm)
    arm.updateAltitude(3.0)
    println(arm)
    arm.toggle()
    println(arm)

    val movingMechanicalArm = MovingMechanicalArm()
    movingMechanicalArm.move(32)
}