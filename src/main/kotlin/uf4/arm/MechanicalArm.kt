package uf4.arm

open class MechanicalArm(var turnedOn: Boolean = false, var openAngle: Double = 0.0, var altitude: Double = 0.0) {
    fun toggle() {
        turnedOn = !turnedOn
    }

    fun updateAngle(angle: Double) {
        if (turnedOn) {
            var totalAngle = openAngle + angle
            if (totalAngle >= 0 || totalAngle <= 360) {
                openAngle = totalAngle
            }
        }
    }

    fun updateAltitude(altitude: Double) {
        if (turnedOn) {
            var totalAltitude = altitude + this.altitude
            if (totalAltitude >= 0) {
                this.altitude = totalAltitude
            }
        }
    }
}

