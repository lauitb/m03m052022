import java.time.LocalDate
import java.time.Period


open class Person(val name : String, val birthDate: LocalDate){
    var test: Int = 4
    fun age() = Period.between(birthDate, LocalDate.now()).years
}

class Student(name : String, birthDate: LocalDate, val startYear: Int) : Person(name, birthDate)


 class Teacher(name:String, birthDate: LocalDate, val sou:Int) : Person(name, birthDate){
fun exemple(){
    super.age()
}

    override fun toString(): String {
        return "name = $name"
    }
}

fun showName (person : Person){

    when(person){
        is Person ->  println(person.name)
        is Student -> println(person.name)
        is Person -> println(person.name)
    }


}
fun main() {
    var swarim = Student("swarim", LocalDate.now(), 2019)
    var laura = Teacher("laura", LocalDate.now(), 300000)
    var person = Person("pepito",LocalDate.now())
    showName(laura)
    showName(swarim)
    showName(person)

}