package uf4.shop

class Bicycle( name:String, var gears:Int,  brand:Brand) : Vehicle(name, brand){
    override fun toString()= "name= $name, gears=$gears, brand=${brand}"
}