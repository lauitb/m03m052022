package uf4.shop

import uf4.shop.Brand

abstract class Vehicle (var name:String, var brand: Brand) {
     abstract override fun toString(): String
}