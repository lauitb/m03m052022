package uf4.shop

class Scooter (name:String, brand:Brand, var power: Double) : Vehicle(name, brand) {
    override fun toString()= "name= $name, brand=${brand}, power=$power "
}