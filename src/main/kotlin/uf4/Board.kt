package uf4

class Rectangle(val height:Double, val width:Double){
    val area get() = height * width
}






class Board(var rectangles : List<Rectangle>){


    fun countRectangles()=  rectangles.size

    fun getTotalArea(): Double {
        var area = 0.0
        for (rectangle in rectangles) {
            area += rectangle.area
        }
        return area
    }
}

fun main() {
    val rectangles = listOf(Rectangle(2.2, 4.5), Rectangle(3.4, 1.5))
    var board = Board(rectangles)
    println(board.countRectangles())
    println(board.getTotalArea())
}