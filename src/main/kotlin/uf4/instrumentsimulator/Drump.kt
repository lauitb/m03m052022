package uf4.instrumentsimulator

import java.lang.IllegalArgumentException

class Drump(var tone:String) : Instrument() {
    init {
        if (tone != "A" && tone != "O" && tone != "U" ){
            throw IllegalArgumentException ()
        }
    }
    override fun makeSounds(times:Int) {
        repeat(times){
            println("T$tone$tone$tone"+"M")
        }

    }

}