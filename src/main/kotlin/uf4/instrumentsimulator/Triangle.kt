package uf4.instrumentsimulator

import java.lang.IllegalArgumentException

class Triangle (var ressonance:Int): Instrument() {
    init {
        if (ressonance !in 1..5) throw IllegalArgumentException()
    }
    override fun makeSounds(times: Int) {
        repeat(times){
            print("T")
            repeat(ressonance){ print("I") }
            println("NC")
        }
    }
}