package uf4.instrumentsimulator

abstract class Instrument() {
    abstract fun makeSounds(times:Int)
}