package uf3




import java.util.*
import kotlin.io.path.Path
import kotlin.io.path.listDirectoryEntries
import kotlin.io.path.moveTo
import kotlin.io.path.name

fun main() {
    val scanner = Scanner(System.`in`)
    val path = Path(scanner.nextLine())
    for(subPath in path.listDirectoryEntries()){
        val newName = subPath.name.uppercase()
        val newPath = subPath.parent.resolve(newName)
        subPath.moveTo(newPath)
    }
}
