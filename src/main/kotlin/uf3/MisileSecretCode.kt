package uf3

import java.util.*
import kotlin.io.path.Path
import kotlin.io.path.writeText

fun main() {
    val scanner = Scanner(System.`in`)
    val userInputValue = scanner.nextLine()
    val homePath = System.getProperty("user.home")
    val path = Path(homePath)
    val secretPath = path.resolve("secret.txt")
    secretPath.writeText(userInputValue)
    println("míssil preparat")
}