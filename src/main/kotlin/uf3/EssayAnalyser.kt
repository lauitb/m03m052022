package uf3


import java.nio.file.Path
import java.util.*
import kotlin.io.path.Path
import kotlin.io.path.readLines

fun main() {
    val scanner = Scanner(System.`in`)
    val path = Path(scanner.nextLine())

    val lines = countLines(path)
    val words = countWords(path)

    println("Número de línies: $lines")
    println("Número de paraules: $words")
}

fun countLines(path: Path) = path.readLines().count()

fun countWords(path: Path): Int {
    val textScanner = Scanner(path)
    var words = 0
    while(textScanner.hasNext()){
        textScanner.next()
        words++
    }
    return words
}
