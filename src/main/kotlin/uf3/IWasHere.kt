package uf3

import java.nio.file.StandardOpenOption
import java.time.LocalDateTime
import kotlin.io.path.Path
import kotlin.io.path.writeText


fun main() {
    val home = System.getProperty("user.home")
    val path  = Path(home, "i_was_here.txt");
    path.writeText("I Was Here: ${LocalDateTime.now()}", options = arrayOf(StandardOpenOption.APPEND))
}
