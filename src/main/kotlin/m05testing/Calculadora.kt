package m05testing

import java.lang.IllegalArgumentException

class Calculadora {
    fun intOperate(s: String): Int {
        val (a, op, b) = s.split(" ")
        return when (op){
            "*" -> a.toInt() * b.toInt()
            "+" -> a.toInt() + b.toInt()
            else -> throw IllegalArgumentException("Invalid arguments")
        }
    }

    fun floatOperate(s: String): Float {
        val (a, op, b) = s.split(" ")
        return when (op){
            "*" -> a.toFloat() * b.toFloat()
            "+" -> a.toFloat() + b.toFloat()
            "/" -> if (b.toFloat() == 0f ){
                throw IllegalArgumentException("No es pot dividir per zero")
            } else a.toFloat() / b.toFloat()
            else -> throw IllegalArgumentException("Invalid arguments")
        }
    }

}