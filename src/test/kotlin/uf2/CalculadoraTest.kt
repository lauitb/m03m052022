package uf2

import m05testing.Calculadora
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import java.time.Duration


internal class CalculadoraTest {
    val calculadora = Calculadora()

    @Nested
    inner class IntOperations {
        @Test
        fun multiplyTest() {
            val result = calculadora.intOperate("2 * 2")
            assertEquals(4, result)
        }

        @Test
        fun addTest() {
            val result = calculadora.intOperate("4 + 3")
            assertEquals(7, result, "no esta sumant els valors 4 i 3 correctament")
        }

        @Test
        fun illegalArgument() {
            assertThrows(IllegalArgumentException::class.java) {
                calculadora.intOperate("4 . 3")
            }

        }
    }

    @Nested
    inner class FloatOperations {


        @Test
        fun `floatOperate - multiplicacio`() {
            val result = calculadora.floatOperate("3.89 * 7.92")
            assertEquals(30.808f, result, 0.001f)
        }

        @Test
        fun `divide test - valid input`() {
            val result = calculadora.floatOperate("3 / 7")
            assertEquals(0.42f, result, 0.01f)
        }

        //@Disabled
        @Test
        fun `divide test invalid input`() {
            assertThrows(IllegalArgumentException::class.java) {
                calculadora.floatOperate("2 / 0")
            }
            //fail<Float>("Invalid input no es pot dividir per 0. Cal solucionar-ho")
        }

        @Test
        fun `divide test timeout`() {
            assertTimeout(Duration.ofMillis(4)) {
                calculadora.floatOperate("123456789 / 9876543231")
            }
        }
    }


}