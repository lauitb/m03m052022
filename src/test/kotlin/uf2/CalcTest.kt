package uf2

import m05testing.Calc
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class CalcTest{
    @Test
    internal fun multiplyBy2() {
        val calculator = Calc()
        var result = calculator.parse("2 * 2")
        assertEquals(4,result)

    }
}