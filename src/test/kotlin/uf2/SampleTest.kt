package uf2

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class SampleTest{

    private val testSample: Sample = Sample()

    @Test
    fun sum() {
        val expected = 43
        assertEquals(expected, testSample.sum(40, 2))
    }


}